package com.devcolibri.databasegikhub.ls10;

abstract class ClassId {
    Integer id;

    ClassId(Integer id){
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
