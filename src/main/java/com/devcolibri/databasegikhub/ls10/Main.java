package com.devcolibri.databasegikhub.ls10;

import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.*;

public class Main {
    private static final String userName = "root";
    private static final String password = "1101";
    private static final String url ="jdbc:mysql://localhost:3306/mydbtest?autoReconnect=true&useSSL=false";

    public static void main (String[] args) throws Exception {

        Connection conn;

        Driver driver = new FabricMySQLDriver();
        DriverManager.registerDriver(driver);

        conn = DriverManager.getConnection(url, userName, password);
        if (!conn.isClosed()) {
            System.out.println("YES!!!");

        }
        Statement stmt = conn.createStatement();

        StorageDatabase storageDatabase = new StorageDatabase();
        storageDatabase.get(Car.class, 10);

        Car car = new Car(50, "Mazda", 1000.5, 500.5);
        storageDatabase.delete(car);
        storageDatabase.save(car);

        conn.close();
    }

}
