package com.devcolibri.databasegikhub.ls10;

import jdk.nashorn.internal.ir.annotations.Ignore;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.*;
import java.lang.annotation.Annotation;
import java.io.Serializable;

public class StorageDatabase implements Storage {

    private Object entity;

    public List<Object> get(Class clazz, Integer id) throws Exception {
        Connection conn = ConnectDataBase.getConnection();
        Statement stmt = conn.createStatement();
        String sClass = clazz.getSimpleName();

        String strSQL = "SELECT * FROM " + sClass + " WHERE ID = " + id.toString();
        ResultSet resultSet = stmt.executeQuery(strSQL);

        List<Object> objList = new ArrayList<Object>();

        /*
        Constructor[] constructors = clazz.getDeclaredConstructors();
        for (Constructor c : constructors) {
            //System.out.print("\t" + getModifiers(c.getModifiers()) +
                    //clazz.getSimpleName() + "(");
            System.out.print(getParameters(c.getParameterTypes()));
           // System.out.print(getParametersName(c.getParameters()));
            System.out.println(") { }");
        }
        */

        while (resultSet.next())
        {
            Object obj = Class.forName(clazz.getName()).newInstance();

            Field[] publicFields = clazz.getFields();
            for (Field field : publicFields) {

                Annotation ann = field.getAnnotation(Ignore.class);
                Ignore ignore = (Ignore)ann;
                if(ignore != null) {
                    continue;
                }

                String sFieldType = getType(field.getType());
                String sNameField = field.getName();

                switch (sFieldType){
                    case ("Integer"):
                        field.set(obj, resultSet.getInt(sNameField));
                        break;
                    case ("Double"):
                        field.set(obj, resultSet.getDouble(sNameField));
                        break;
                    case ("String"):
                        field.set(obj, resultSet.getString(sNameField));
                        break;
                }

                System.out.println("Имя: " + sNameField);
                System.out.println("Тип: " + sFieldType);

            }

            objList.add(obj);
            System.out.println(obj.toString());
        }

        conn.close();
        return objList;

    }

    public List list(Class clazz) throws Exception {

        List<String>retList = new ArrayList <String>();

        Connection conn = ConnectDataBase.getConnection();
        Statement stmt = conn.createStatement();

        String sClass = clazz.getSimpleName();

        String strSQL = "SELECT * FROM " + sClass;
        ResultSet resultSet = stmt.executeQuery(strSQL);

        while (resultSet.next())
        {
            String id = resultSet.getString("id");
            retList.add(id);
        }
        conn.close();

        return retList;
    }

    public boolean delete(Object entity ) throws Exception {
        Connection conn = ConnectDataBase.getConnection();
        Statement stmt = conn.createStatement();

        String sClass = entity.getClass().getSimpleName();

        Field idFiels = entity.getClass().getSuperclass().getDeclaredField("id");
        idFiels.setAccessible(true);
        String id = (String)idFiels.get(entity).toString();

        System.out.println("DELETE FROM " + sClass + " WHERE ID = "+id);
        String strSQL = "DELETE FROM " + sClass + " WHERE ID = "+id;

        boolean retVal = stmt.execute(strSQL);;
        conn.close();

        return retVal;
    }

    public void save(Object entity) throws Exception {
        Connection conn = ConnectDataBase.getConnection();
        Statement stmt = conn.createStatement();

        String sClass = entity.getClass().getSimpleName();

        String strSQL = "INSERT INTO "  +sClass + " (";
        String sqlValues = "";

        Field idField = entity.getClass().getSuperclass().getDeclaredField("id");
        strSQL += "id";

        idField.setAccessible(true);
        String id = (String)idField.get(entity).toString();
        sqlValues += id;

        Field[] publicFields = entity.getClass().getFields();
        boolean isFirst = true;
        for (Field field : publicFields) {
            Annotation ann = field.getAnnotation(Ignore.class);
            Ignore ignore = (Ignore)ann;
            if(ignore != null) {
                continue;
            }

            String sNameField = field.getName();
            String sFieldType = getType(field.getType());


            strSQL += ", ";
            sqlValues += ", ";

            strSQL += sNameField;

            field.setAccessible(true);
            String val = (String)field.get(entity).toString();
            System.out.println(sFieldType);

            sqlValues += (sFieldType.equals("String")? "'":"")+ val+(sFieldType.equals("String")? "'":"");

        }
        strSQL += ") VALUES (" + sqlValues + ")";
        System.out.println(strSQL);
        stmt.execute(strSQL);
        conn.close();

    }

    static String getType(Class clazz) {
        String type = clazz.isArray()
                ? clazz.getComponentType().getSimpleName()
                : clazz.getSimpleName();
        if (clazz.isArray()) type += "[]";
        return type;
    }

    static String getParameters(Class[] params) {
        String p = "";
        for (int i = 0, size = params.length; i < size; i++) {
            if (i > 0) p += ", ";
            //String fieldName = params[i].getName();
            p += getType(params[i]) + " param" + i;
            //p += fieldName;
        }
        return p;
    }

    static String getParametersName(Parameter[] parameters){
        String p = "";
        for (int i = 0, size = parameters.length; i < size; i++) {
            if (i > 0) p += ", ";
            //String fieldName = params[i].getName();
            p += parameters[i].getName();
            System.out.println(parameters[i].getName());
            System.out.println(parameters[i].toString());
            //p += fieldName;
        }
        return p;
    }

}
