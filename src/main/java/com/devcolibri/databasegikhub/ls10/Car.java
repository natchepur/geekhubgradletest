package com.devcolibri.databasegikhub.ls10;

import jdk.nashorn.internal.ir.annotations.Ignore;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Car extends ClassId{
    public String model;
    public Double mileage;
    @Ignore
    public Double engineVolume;

    Car(Integer id, String model, Double mileage, Double engineVolume) {
        super(id);
        this.model = model;
        this.mileage = mileage;
        this.engineVolume = engineVolume;
    }

    Car() {
        super(1);
        this.model = "";
        this.mileage = Double.valueOf(0);
        this.engineVolume = Double.valueOf(0);
    }

    /*
    public List<Car> get(Class clazz, Integer id) throws Exception {
        //return null;
        Connection conn = ConnectDataBase.getConnection();
        Statement stmt = conn.createStatement();

        String strSQL = "SELECT * FROM "+clazz.getSimpleName()+" WHERE ID = " + id.toString();
        ResultSet resultSet = stmt.executeQuery(strSQL);

        List<Car> carsList = new ArrayList <Car>();

        while (resultSet.next())
        {
            String model = resultSet.getString("model");
            Double mileage = resultSet.getDouble("mileage");
            Double engineVolume = resultSet.getDouble("engineVolume");

            carsList.add(new Car(id,model,mileage,engineVolume));
        }
        conn.close();
        return carsList;
    }

    public List list(Class clazz) throws Exception {
        List<String>modelList = new ArrayList <String>();

        Connection conn = ConnectDataBase.getConnection();
        Statement stmt = conn.createStatement();

        String strSQL = "SELECT * FROM "+clazz.getSimpleName();
        ResultSet resultSet = stmt.executeQuery(strSQL);

        while (resultSet.next())
        {
            String model = resultSet.getString("model");
            modelList.add(model);
        }
        conn.close();

        return modelList;
    }

    public boolean delete(Object car) throws Exception {
        return false;
    }

    public <T> void save(T entity) throws Exception {

    }
    */
}
