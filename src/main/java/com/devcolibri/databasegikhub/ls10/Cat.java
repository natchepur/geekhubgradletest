package com.devcolibri.databasegikhub.ls10;

import java.util.List;

public class Cat extends ClassId {
    String name;
    Double age;

    Cat(Integer id, String name, Double age) {
        super(id);
        this.name = name;
        this.age = age;
    }

}
