package com.devcolibri.databasegikhub.ls10;

import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectDataBase {

    private static final String userName = "root";
    private static final String password = "1101";
    private static final String url = "jdbc:mysql://localhost:3306/mydbtest?autoReconnect=true&useSSL=false";
    private static Connection conn;

    static Connection getConnection() {

        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);

            conn = DriverManager.getConnection(url, userName, password);
            return conn;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }
}
