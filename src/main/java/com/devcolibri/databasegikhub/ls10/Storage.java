package com.devcolibri.databasegikhub.ls10;

import java.util.List;

import static javafx.scene.input.KeyCode.T;

public interface Storage {

    <T> T get(Class clazz, Integer id) throws Exception;

    List list(Class clazz) throws Exception;

    <T> boolean delete(T entity) throws Exception;

    <T> void save(T entity) throws Exception;

}
